FROM ruby:3.2.2
ENV BUNDLER_VERSION=2.2.16
ENV HOST=postgres
RUN apt-get update && apt-get install -y apt-transport-https
RUN apt-get update -qq && apt-get install -y nodejs npm postgresql-client
RUN apt-get install -y graphviz
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update -qqy && apt-get install -qqyy yarn nodejs postgresql postgresql-contrib libpq-dev cmake
RUN npm install n -g
RUN n stable
RUN npm install -g yarn
RUN yarn add bootstrap
WORKDIR /leobotics
COPY Gemfile /leobotics/Gemfile
COPY Gemfile.lock /leobotics/Gemfile.lock
RUN bundle install
RUN bundle exec rails webpacker:install

RUN rails assets:precompile


# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000
WORKDIR /leobotics

# Configure the main process to run when running the image
#CMD ["rails", "server", "-b", "0.0.0.0"]
CMD ["/bin/sh"]


# docker build . -t "app-test"
#CMD ["/bin/sh"]
#docker run -it -p 127.0.0.1:3000:3000 -v .:/app-test app-test
#command dans le docker rails s -b 0.0.0.0