# Leobotics.fr

![Pipeline status](https://gitlab.com/hakim.ghanem/leobotics/badges/main/pipeline.svg)
![Coverage](https://gitlab.com/hakim.ghanem/leobotics/badges/main/coverage.svg)

## Tools

NODE v20.10.0
RUBY 3.2.2
RAILS 7.0.7.2

Maildev for local smtp server: TODO

### Installation guide

If you use `asdf`, add `ruby` and `node` plugins.

Install, `ruby@3.2.2` and `nodejs@20.10.0`.
Install the dependencies: `bin/bundle i` and `yarn install`.
Init the database: `bin/rails db:create && bin/rails db:migrate && bin/rails db:seed`.
Precompile the assets: `bin/rails assets:precompile`

Maildev: TODO

You're good to go: `bin/dev` to run the app, and browse `localhost:3000`.

## Docker setup

### Docker install

Make sure you have the [docker engine](https://docs.docker.com/engine/) and [docker-compose](https://docs.docker.com/compose/) installed on your computer.

#### Docker usage

This is to build the different containers to have the whole app running.

* Build the docker containers
```bash
docker-compose build
```

* Boot the app
```bash
docker-compose up
```

* Go to `localhost:3000` and the app should be running.

#### Webapp Container

This is to build the only the ROR app container. It either uses a local or external database.

Build container  `docker build . -t "leobotics"`
Run container and get a prompt: `docker run -it -p 127.0.0.1:3000:3000 -v .:/leobotics leobotics`
Start app: `rails s -b 0.0.0.0`

## Development

As mentionned earlier, running `bin/dev` should start everything you need to start developing. You will only need to restart the web server (`rails s` command) if you edit the rails config (routes, initializers, environments...) or if you add a new gem, this kind of stuff.

It is encouraged to regularly use the `rubocop` gem to make sure your code is readable:

- `rubocop` prints all the issues with your code.
- `rubocop -a` prints all the issues and fixes a few of them. This should not break anything.
- `rubocop -A` prints all the issues and fixes as much issues as it can. **It may break your code**.

If you think a warning is not legit, you have two options:

- dig into `rubocop` and edit the `.rubocop.yml` to add/edit/remove a rule;
- or run `rubocop --auto-gen-config` to delay the previous step (this will update `.rubocop_todo.yml` and hide the warning).

<!-- ## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/hakimghanem/leobotics.git
git branch -M main
git push -uf origin main
```


## Test and Deploy

### SAT Rubocop 
To run rubocop type pour build  `bundle exec rubocop`
to see all the checkers, go check the `.rubocop.yml` or `.rubocop_todo.yml`

### Unit test 
To run rubocop type pour build  `bundle exec rspec`
to check the coverage of the unit test, the gem simplecov is installed so type `open coverage/index.html`
-->