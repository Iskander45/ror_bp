class AddressesController < ApplicationController
    before_action :set_account
    before_action :set_address, only: %i[ update destroy ]

    def new
        @address = Address.new
        @address.account_id = current_user.account.id
    end

    def show
    end

    def update
        respond_to do |format|
          if @address.update(address_params)
            format.json { render :show, status: :ok, location: @address }
          else
            format.html { render :edit, status: :unprocessable_entity }
            format.json { render json: @address.errors, status: :unprocessable_entity }
          end
        end
    end

    # DELETE /addresss/1 or /addresss/1.json
    def destroy
        @address.destroy

        respond_to do |format|
          format.html { redirect_to edit_account_path(@address.account), notice: 'Adresse supprimée avec succès.' }
          format.json { head :no_content }
        end
    end
    
    def edit
      @address = Address.find(params[:id])
      @addresses = Address.where(account_id: @address.account_id)
    end

    def create
        @account = Account.find(params[:id])
        @address = Address.new(address_params)
        @address.account_id = @account.id

        if @address.save
          redirect_to edit_account_path(current_user.account), notice: 'Adresse créée avec succès.'
        else
          render :new
        end
    
      end

    private

    def set_account
      if user_signed_in?
        @account = Account.find_by(user_id: current_user.id)
      end
    end

    def set_address
        @address = Address.find(params[:id])
    end

    def address_params
        params.require(:address).permit(:street, :street_two, :city, :country, :state, :pc)
    end
end