# app/controllers/user_managment_controller.rb
class Admin::UserManagmentController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_admin!
  before_action :set_user, only: %i[edit_user delete_user]

  def index
  end

  def manage_user
    # @users = User.where(role: :client)
    @users = User.all 
    @roles = @users.pluck(:role).uniq
  end

  def new_user
    @user = User.new
  end

  def create_user
    @user = User.new(user_params)
    if @user.save
      redirect_to user_managment_path, notice: 'User created successfully.'
    else
      render :new
    end
  end

  def delete_user
    @user.transaction do
      @user.destroy
    end
    redirect_to user_managment_path, success: 'Compte supprimé'
  end

  def edit_user
  end

  private

  def authorize_admin!
    authorize! :manage, :all
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :role)
  end

  def set_user
    @user = User.find(params[:id])
  end
end
