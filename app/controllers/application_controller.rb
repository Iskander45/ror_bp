class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_devise_parameters, if: :devise_controller?

  def configure_devise_parameters
    devise_parameter_sanitizer.permit(:sign_up) do |u|
      u.permit(:username, :email, :password, :password_confirmation)
    end
  end

  def after_sign_out_path_for(_resource_or_scope)
    # Specify your custom path here
    root_path
  end

  rescue_from CanCan::AccessDenied do |exception|
    puts exception
    redirect_to new_user_session_path, notify: 'Acces interdit'
  end
end
