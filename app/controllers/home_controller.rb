class HomeController < ApplicationController

  before_action :set_account, if: :user_signed_in?

  def index
  end

  private

    def set_account
      @account = Account.find_by(user_id: current_user.id)
    end

end
