class UsersController < ApplicationController
  before_action :authenticate_user!, except: %i[create reset_password]
  before_action :set_user, only: %i[edit update destroy reset_password]


  def new
    @user = User.new
  end

  def edit
    @account = Account.find_by(user_id: @user.id)
  end

  def show
  end

  def create
    if user_signed_in? && current_user.role == 'admin'
      @user_admin = current_user
    else
      @user_admin = nil
    end
    generated_password = Devise.friendly_token.first(6)
    _raw, hashed = Devise.token_generator.generate(User, :reset_password_token)
    @user = User.new(user_params)
    @user.transaction do
      @user.password = generated_password
      @user.reset_password_token = hashed
      if params[:chosen_role].present?
        @user.role = params[:chosen_role]
      end      
      @user.reset_password_sent_at = Time.now.utc
      @user.confirmed_at = Time.now.utc
      if @user_admin
        if @user.valid?
          @user.save!
          redirect_to admin_manage_user_path(), flash: { success: "Membre supplémentaire créé avec l'adresse #{@user.email}, l'utilisateur va recevoir un email pour confirmer la création de son compte." }
        else
          redirect_to admin_manage_user_path(), flash: { danger: 'Membre non créé' }
        end
      else
        if @user.valid?
          @user.save!
          sign_in(@user)
          redirect_to root_path, flash: { success: "Inscription réussie" }
        else
          redirect_to root_path, flash: { danger: 'Membre non créé' }
        end
      end
    end
  end  

  def update
    
    if @user.update(user_params)
      redirect_to admin_manage_user_path(), flash: { success: 'Membre modifié avec succèes' }
    else
      redirect_to admin_manage_user_path(), flash: { success: 'Membre non modifié' }
    end
  end

  def destroy
    @user.transaction do
      @user.destroy
    end
    redirect_to admin_manage_user_path(), flash: { success: 'Client supprimé' }
  end
  
  def reset_password
    @user.send_reset_password_instructions
    if current_user.admin?
      redirect_to admin_manage_user_path(), flash: { success: 'Email envoyé avec succès' }
    else
      redirect_to root_path, flash: { success: 'Email envoyé avec succès' }
    end
  end
  

  private
  def user_params
    params.require(:user).permit(:username, :email, :password, :password_confirmation ,:role, account_attributes: [:id, :firstname, :lastname, :phone_number])
  end  

  def set_user
    @user = User.find(params[:id])
  end


end

