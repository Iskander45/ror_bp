module ApplicationHelper
  def own_post?(post)
    # check if it's the user's post
    post.user_id == current_user.id?
  end
end
