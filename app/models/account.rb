class Account < ApplicationRecord
    belongs_to :user
    has_one_attached :image, dependent: :destroy
    has_many :addresses, dependent: :destroy

    # validates :phone_number, format: { with: /\A(06|07)\d{8}\z/, message: "The phone number must start with 06 or 07 and have 10 digits in total" }

end
