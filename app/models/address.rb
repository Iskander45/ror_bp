class Address < ApplicationRecord
    belongs_to :account, optional: true

    # Présence
    validates :street, :city, :country, :state, presence: true
  
    # Longueur
    validates :street, length: { minimum: 3, maximum: 255 } 
    validates :street_two, length: { maximum: 255 }, allow_blank: true 
    validates :city, length: { maximum: 50 }, presence: true
    validates :state, length: { maximum: 50 }, presence: true

    # Format
    validates :pc, format: { with: /\A\d{5}\z/, message: "doit comporter 5 chiffres" }, presence: true

end
