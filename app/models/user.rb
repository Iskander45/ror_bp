class User < ApplicationRecord
  enum role: {
    client: 'client',
    admin: 'Administrateur',
    comm: 'communication',
    sales: 'vendeur',
    sales_rep: 'commercial',
    robot_agent: 'robotique'
  }
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable, :trackable
  after_create :create_associated
  attr_accessor :login
  has_one :account, dependent: :destroy
  accepts_nested_attributes_for :account
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  # validates :password, presence: true
  # validates :password_confirmation, presence: true
  validates :username, presence: true, uniqueness: { case_sensitive: false },
                       format: { with: /\A[a-zA-Z0-9 _\.]*\z/ }
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_hash).where('lower(username) = :value OR lower(email) = :value',
                                      value: login.downcase).first
    else
      where(conditions.to_hash)
    end
  end

  private

  def create_associated
    self.account = Account.create()
  end

end
