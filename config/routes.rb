Rails.application.routes.draw do
  resources :accounts
  resources :users, only: %i[new create edit update destroy]
  devise_for :users

  # dashboard user_managment
  # config/routes.rb
  resources :users do
    member do
      post 'reset_password'
    end
  end

  namespace :admin do
    resources :user_managment, only: [:index] do
      member do
        get 'new_user', to: 'user_managment#new_user', as: :user_managment_new_user
        get 'create_user', to: 'user_managment#create_user', as: :user_managment_create_user
        get 'edit_user/:id', to: 'user_managment#edit_user', as: :user_managment_edit_user
        get 'delete_user/:id', to: 'user_managment#delete_user', as: :user_managment_delete_user
      end
    end
    get 'manage_user', to: 'user_managment#manage_user', as: :manage_user
  end

  
  resources :addresses
  root 'home#index'

  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
