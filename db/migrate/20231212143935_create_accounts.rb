class CreateAccounts < ActiveRecord::Migration[7.0]
  def change
    create_table :accounts do |t|
      t.string :firstname
      t.string :lastname
      t.string :phone_number
      t.integer :addresse
      t.integer :amount_of_orders
      t.string :last_connexion
      t.string :image

      t.belongs_to :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
