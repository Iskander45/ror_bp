class CreateAddresses < ActiveRecord::Migration[7.0]
  def change
    create_table :addresses do |t|

      t.string :street
      t.string :street_two
      t.string :city
      t.string :country
      t.string :state
      t.integer :pc
      t.references :account, null: true, foreign_key: true 
      t.timestamps
    end
  end
end

