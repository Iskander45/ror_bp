# This file should contain all the record creation needed to seed the database with its default
# values. The data can then be loaded with the bin/rails db:seed command (or created alongside the
# database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

# #Creating user

require 'faker'

admin = User.create!(
  email: 'admin@test.com',
  username: 'admin',
  password: '123456',
  role: 'admin',
  confirmed_at: Time.now.utc
)

admin.account.update(
  firstname: 'John',
  lastname: 'Doe',
  phone_number: '0683748329',
)

admin.account.addresses << Address.new(
  street: '123 Main Street',
  street_two: 'Apt 4B',
  city: 'New York',
  country: 'USA',
  state: 'NY',
  pc: 10001
)

admin.save!

['comm', 'sales', 'sales_rep', 'robot_agent'].each do |role|
  3.times do |i|
    user = User.create!(
      email: "#{role}#{i}@test.com",
      username: Faker::Internet.unique.username,
      password: Faker::Internet.password(min_length: 6, max_length: 12),
      role: role,
      confirmed_at: Time.now.utc
    )

    user.account.update(
      firstname: Faker::Name.first_name,
      lastname: Faker::Name.last_name,
      phone_number: Faker::PhoneNumber.cell_phone
    )

    user.account.addresses << Address.new(
      street: Faker::Address.street_address,
      street_two: Faker::Address.secondary_address,
      city: Faker::Address.city,
      country: Faker::Address.country,
      state: Faker::Address.state,
      pc: Faker::Address.zip_code
    )

    user.save!
  end
end


0.times do |i|
  user = User.create!(
    email: Faker::Internet.unique.email,
    username: Faker::Internet.unique.username,
    password: '123456',
    role: 'client',
    confirmed_at: Time.now.utc
  )

  user.account.update(
    firstname: Faker::Name.first_name,
    lastname: Faker::Name.last_name,
    phone_number: Faker::PhoneNumber.cell_phone
  )

  user.account.addresses << Address.new(
    street: Faker::Address.street_address,
    street_two: Faker::Address.secondary_address,
    city: Faker::Address.city,
    country: Faker::Address.country,
    state: Faker::Address.state,
    pc: Faker::Address.zip_code
  )

  user.save!
end
