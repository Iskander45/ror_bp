require 'rails_helper'

RSpec.describe AccountsController, type: :controller do
    
    describe "GET #index" do
    it "assigns @accounts with all custom accounts associated with users" do
      # Crée 3 utilisateurs et leurs comptes associés
      create(:user) do |user|
        create(:account, user: user)
      end
  
      create(:user) do |user|
        create(:account, user: user)
      end
  
      create(:user) do |user|
        create(:account, user: user)
      end
  
      get :index
      accounts = User.all.map(&:account)
      expect(assigns(:accounts).map(&:id)).to match_array(accounts.map(&:id))
      expect(assigns(:accounts).count).to eq(3)
    end
  end
    

    describe "GET #show" do
        let(:user) { create(:user) }
        let(:account) { create(:account, user: user) }

        it "assigns @user and @account" do
            get :show, params: { id: account.id }
            expect(assigns(:user)).to eq(user)
            expect(assigns(:account)).to eq(account)
        end
    end


    describe "GET #edit" do
        let(:user) { create(:user) }
        let(:account) { create(:account, user: user) }

        it "assigns @account and @user" do
            get :edit, params: { id: account.id }
            expect(assigns(:account)).to eq(account)
            expect(assigns(:user)).to eq(user)
        end
    end

end
