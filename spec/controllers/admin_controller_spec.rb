require 'rails_helper'

RSpec.describe AdminController, type: :controller do
  let(:admin_user) { create(:user, role: 'admin') }
  let(:user) { create(:user, role: 'client') }

  before do
    sign_in admin_user
  end

  describe "GET #index" do
    it "returns a success response" do
      get :index
      expect(response).to be_successful
    end
  end

  describe "GET #admin_account" do
    it "returns a success response" do
      get :admin_account
      expect(response).to be_successful
    end
  end


  describe "POST #create_user" do
  let(:valid_user_params) { attributes_for(:user, role: 'client') }

  context "with valid parameters" do
    it "creates a new user" do
      expect {
        post :create_user, params: { user: valid_user_params }
      }.to change(User, :count).by(1)
    end

    it "redirects to the admin path" do
      post :create_user, params: { user: valid_user_params }
      expect(response).to redirect_to(admin_path)
    end

    it "sets a flash notice" do
      post :create_user, params: { user: valid_user_params }
      expect(flash[:notice]).to eq('User created successfully.')
    end
  end
end


  describe "DELETE #delete_user" do
    it "destroys the requested user" do
      user_to_delete = create(:user, role: 'client')
      expect {
        delete :delete_user, params: { id: user_to_delete.id }
      }.to change(User, :count).by(-1)
    end

    it "redirects to the admin path" do
      user_to_delete = create(:user, role: 'client')
      delete :delete_user, params: { id: user_to_delete.id }
      expect(response).to redirect_to(admin_path)
    end
  end

  describe "GET #edit_user" do
    it "returns a success response" do
      get :edit_user, params: { id: user.id }
      expect(response).to be_successful
    end
  end
end
