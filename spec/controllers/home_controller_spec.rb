require 'rails_helper'

RSpec.describe HomeController, type: :controller do
  describe "GET #index" do
    context "when user is signed in" do
      let(:user) { create(:user) }

      before do
        sign_in user
      end

      it "returns a success response" do
        get :index
        expect(response).to be_successful
      end

      it "assigns @account if user is signed in" do
        account = create(:account, user: user)
        get :index
        expect(assigns(:account)).to eq(account)
      end
    end

    context "when user is not signed in" do
      it "returns a success response" do
        get :index
        expect(response).to be_successful
      end

      it "does not assign @account if user is not signed in" do
        get :index
        expect(assigns(:account)).to be_nil
      end
    end
  end
end
