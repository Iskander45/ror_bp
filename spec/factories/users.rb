require 'faker'
FactoryBot.define do
  factory :user, class: User do
    email { Faker::Internet.email }
    username { Faker::Games::Heroes.name }
    password { '123456' }
    confirmed_at { Faker::Date.backward }
  end

  factory :user_mail, class: User do
    email { 'test@test.com' }
    username { Faker::Games::Heroes.name }
    password { '123456' }
    confirmed_at { Faker::Date.backward }
  end

  factory :user_admin, class: User, parent: :user do
    after :create do |user|
      user.role = 'admin'
      user.save!
    end
  end
end
