require 'rails_helper'

RSpec.describe Ability, type: :model do
  describe "initialize" do
    let(:user) { User.new }

    context "when the user is an admin" do
      before { user.role = 'admin' }

      it "should be able to manage all" do
        ability = Ability.new(user)
        expect(ability.can?(:manage, :all)).to be_truthy
      end
    end

    context "when the user is a comm" do
      before { user.role = 'comm' }

      it "should have specific permissions for comm" do
        # Add your specific permissions test for comm here
      end
    end

    context "when the user is a sales" do
      before { user.role = 'sales' }

      it "should have specific permissions for sales" do
        # Add your specific permissions test for sales here
      end
    end

    context "when the user is a sales_rep" do
      before { user.role = 'sales_rep' }

      it "should have specific permissions for sales_rep" do
        # Add your specific permissions test for sales_rep here
      end
    end

    context "when the user is a robot_agent" do
      before { user.role = 'robot_agent' }

      it "should have specific permissions for robot_agent" do
        # Add your specific permissions test for robot_agent here
      end
    end

    context "when the user is a client" do
      before { user.role = 'client' }

      it "should have specific permissions for client" do
        # Add your specific permissions test for client here
      end
    end

  end
end

