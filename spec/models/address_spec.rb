require 'rails_helper'

RSpec.describe Address, type: :model do
  describe "validations" do
    let(:address) { Address.new }

    it "is valid with valid attributes" do
      address.street = "123 Main St"
      address.city = "City"
      address.country = "Country"
      address.state = "State"
      address.pc = "12345"
      expect(address).to be_valid
    end

    it "is not valid without a street" do
      expect(address).not_to be_valid
      expect(address.errors[:street]).to include("can't be blank")
    end

    it "is not valid without a city" do
      expect(address).not_to be_valid
      expect(address.errors[:city]).to include("can't be blank")
    end

    it "is not valid without a country" do
      expect(address).not_to be_valid
      expect(address.errors[:country]).to include("can't be blank")
    end

    it "is not valid without a state" do
      expect(address).not_to be_valid
      expect(address.errors[:state]).to include("can't be blank")
    end

    it "is not valid without a pc" do
      expect(address).not_to be_valid
      expect(address.errors[:pc]).to include("can't be blank")
    end

    it "is not valid with a pc that doesn't have 5 digits" do
      address.pc = "1234"
      expect(address).not_to be_valid
      expect(address.errors[:pc]).to include("doit comporter 5 chiffres")
    end

    it "is valid with a pc that has exactly 5 digits" do
      address.pc = "12345"
      expect(address).to be_valid
    end

    it "is not valid with an optional street_two that exceeds 255 characters" do
      address.street = "123 Main St"
      address.street_two = "x" * 256
      address.city = "City"
      address.country = "Country"
      address.state = "State"
      address.pc = "12345"
      expect(address).not_to be_valid
    end


    it "is not valid with an optional state that exceeds 50 characters" do
      address.street = "123 Main St"
      address.city = "City"
      address.country = "Country"
      address.state = "x" * 51
      address.pc = "12345"
      expect(address).not_to be_valid
    end
  end
end
