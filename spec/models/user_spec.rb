require 'rails_helper'

RSpec.describe User, type: :model do
  context 'Creation' do
    it 'check that user was created' do
      create(:user)
      expect(User.count).to eq(1)
    end

    it 'check default user is a client' do
      u = create(:user)
      expect(u.role == 'client').to be true
    end

    context 'Validation' do
      it 'should validate that two emails are not the same' do
        user2 = User.new(email: 'test@test.com', username: '2', password: '123456')
        user2.skip_confirmation!
        expect do
          user2.save!
        end.to raise_error(ActiveRecord::RecordInvalid,
                           'Validation failed: Email has already been taken')
        expect(user2.valid?).to be false
      end

      it 'should validate password length' do
        user = User.new(email: '1@test.com', username: '1', password: '12356')
        user.validate
        expect(user.errors.messages).to include(:password)
        expect(user.valid?).to be false
      end

      it ' should be admin' do
        u = create(:user_admin)
        expect(u.role == 'admin').to be true
      end

      it "defines the correct roles enum" do
        # Crée un nouvel utilisateur avec un rôle de client
        user = User.new(role: :client)
    
        # Vérifie que le rôle de l'utilisateur est correct
        expect(user.role).to eq('client')
    
        # Vérifie que les rôles possibles sont corrects
        expect(User.roles.keys).to match_array(['client', 'admin', 'comm', 'sales', 'sales_rep', 'robot_agent'])
      end
    end
  end

  describe "self.find_first_by_auth_conditions" do
    it "should find a user by username or email when :login is provided" do
      # Créez un utilisateur de test avec un nom d'utilisateur et un e-mail
      user = User.create(username: 'john_doe', email: 'john@example.com', password: 'password')

      # Appelez la méthode find_first_by_auth_conditions avec :login
      result = User.find_first_by_auth_conditions({ login: 'john_doe' })

      # Vérifiez que le résultat est l'utilisateur créé
      expect(result).to eq(user)
    end

    it "should find users based on other conditions when :login is not provided" do
      # Créez plusieurs utilisateurs avec différentes conditions
      user1 = User.create(username: 'user1', email: 'user1@example.com', password: 'password', role: 'client')
      user2 = User.create(username: 'user2', email: 'user2@example.com', password: 'password', role: 'admin')
    
      # Appelez la méthode find_first_by_auth_conditions avec d'autres conditions
      result = User.find_first_by_auth_conditions({ role: 'admin' })
    
      # Vérifiez que le résultat correspond à l'un des utilisateurs créés
      expect([user1, user2]).to include(result)
    end

    it "should return nil when no matching user is found" do
      # Appelez la méthode find_first_by_auth_conditions avec des conditions non valides
      result = User.find_first_by_auth_conditions({ login: 'nonexistent_user' })

      # Vérifiez que le résultat est nil (aucun utilisateur trouvé)
      expect(result).to be_nil
    end
  end

end
